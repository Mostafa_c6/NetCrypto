import asyncio

from utils.message.message import Message
import utils.message.format as msg_format


def handle_response(message):
    return


class NetCryptoClientProtocol(asyncio.Protocol):
    def __init__(self, message, loop):
        self.message = message
        self.loop = loop

    def connection_made(self, transport):
        print("Data to send {}".format(self.message))

        transport.write(self.message)

    def data_received(self, data):
        response = msg_format.loads(data)
        print('Data received: {!r}'.format(response))

    def connection_lost(self, exc):
        print('The server closed the connection')
        # Stop the event loop
        self.loop.stop()


def run_proto(message):
    loop = asyncio.get_event_loop()
    coro = loop.create_connection(lambda: NetCryptoClientProtocol(message, loop),
                                  '127.0.0.1', 8888)
    loop.run_until_complete(coro)
    loop.run_forever()
    loop.close()


if __name__ == '__main__':
    run_proto(Message(request="register",
                      auth=dict(username='test', password='test'), pubkey=b"public bytes"))
