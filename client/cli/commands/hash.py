from client.proto import run_proto
from utils.crypto.loading import load_public_key
from utils.message.message import Message
import utils.message.format as msg_format

from utils.crypto.encryption import public_key_encryption
import click


@click.command(help="Request a hash from server.")
@click.option('--username', '-u', prompt=True)
@click.option('--password', '-p', prompt=True)
@click.option('--file', '-f', 'hash_file', help="file path to hash", type=click.File('rb'))
@click.option('--text', '-t', 'text', help="inline text to hash")
@click.option('--server_key', prompt=True, help="Path to server public key file",
              type=click.Path(exists=True, file_okay=True, dir_okay=False))
def hash_service(username, password, hash_file, text, server_key):
    if hash_file:
        hash_value = hash_file.read()  # reads binary
    else:
        hash_value = text.encode()
    print("content", hash_value)
    try:
        public_server = load_public_key(server_key)
    except ValueError:
        raise click.UsageError("Unable to load  key(s)'")

    msg = create_hash_message(username, password, hash_value)

    # proto is run in plain text
    serilized_msg = msg_format.dumps(msg)

    run_proto(serilized_msg)


def create_hash_message(username, password, hash_value):
    return Message(action='hash', username=username, password=password,
                   hash=hash_value)


if __name__ == '__main__':
    hash_service()
