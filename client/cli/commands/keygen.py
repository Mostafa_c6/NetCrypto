from utils.crypto.genrsa import generate_private_key
from utils.crypto.serialization import serialize_private_key, serialize_public_key
import click
from pathlib import Path


def generate_serialized_keys():
    # todo : remove insecure size for production
    private_key = generate_private_key(public_exponent=65537, key_size=1024)
    public_key = private_key.public_key()

    return serialize_private_key(private_key).decode(), \
           serialize_public_key(public_key).decode()


def save_to_path(path, content, mode='w+'):
    with path.open(mode) as f:
        f.write(content)


@click.command(help="Generate a 1024bit RSA key pair.")
@click.option('--path', '-p', type=click.Path(exists=True, file_okay=False, dir_okay=True),
              help="Directory to save the key pair.", prompt=True)
def generate_keypair(path):
    p = Path(path)

    private, public = generate_serialized_keys()

    save_to_path(p / 'private.pem', private)
    click.echo(click.style('Private key created', fg='green'))

    save_to_path(p / 'public.pem', public)
    click.echo(click.style('Public key created', fg='green'))


if __name__ == '__main__':
    generate_keypair()
