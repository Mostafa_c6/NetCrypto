import click
import utils.message.format as msg_format
from utils.message.message import Message
from utils.crypto.loading import load_public_key
from utils.crypto.serialization import serialize_public_key
from client.proto import run_proto


@click.command(help="Register to server.")
@click.option('--username', '-u', prompt=True)
@click.option('--password', '-p', prompt=True)
@click.option('--client_key', prompt=True, help="Path to client public key file",
              type=click.Path(exists=True, file_okay=True, dir_okay=False))
def register(username, password, client_key):
    try:
        client_pubkey = load_public_key(client_key)
    except ValueError:
        raise click.UsageError("Unable to load  key(s)'")

    msg = create_register_message(password, client_pubkey, username)

    # proto is run in plain text
    serilized_msg = msg_format.dumps(msg)

    run_proto(serilized_msg)


def create_register_message(password, client_pubkey, username):
    return Message(action='register', username=username, password=password,
                   public_key=serialize_public_key(client_pubkey))


if __name__ == '__main__':
    print(register())
