"""
Command Line Interface for client side of protocol
"""

from client.cli.commands.register import register as register_command
from client.cli.commands.keygen import generate_keypair as keygen_command
from client.cli.commands.hash import hash_service as hash_command

import click


@click.group()
def entry_point():
    pass


entry_point.add_command(register_command, name="register")
entry_point.add_command(keygen_command, name="keygen")
entry_point.add_command(hash_command, name="hash")

if __name__ == '__main__':
    entry_point()
