# NetCrypto

Networking and cryptography project for IT Engineering course

Project at a top level is divided into three major parts:
+ client
    + cli
    + pair
+ server
   + db
   + pair
+ utils
   + crypto
   + message

`utils` contain wrappers and utility functions and classes used and usually shared between client and server.

Both `client` and `server` packages contain a `pair` directory which contains respective asymmetric key pairs in `pem` format.

Programs supports multiple users with the help of python [`asyncio`](https://docs.python.org/3/library/asyncio.html) which provides asynchronous programming and provides wrapper for common network protocols, TCP included.

`asyncio` make use of coroutines and non-blocking I/O operations.
Two high-level abstractions of `asyncio`, Protocol and Transport are used in this project.

Users credentials `(username,passowrd,private key)` are saved in a Sqlite3 database on the server side.




The Program flow :
1. Client used command line interface to get user request (`client/cli/cli.py`)

 
2. Based on clients request, corresponding handler is called and creates proper request message (`client/cli/commands/`). 
commands are :
    + `register`
    + `hash`
    + `keygen` : a utility to generate RSA keypair for clients
   
   read more on cli help section:
   `python client/cli/cli.py --help`
   
3. Client pushes the message down the async protocol to send over the network
4. Server receives the request, based on `action` type in request, figures out the proper handler. Server uses a new `asyncio.protocol` for each user. (`server/main.py`)
5. handler is called, operations are done accordingly
    + `register` : Creates a User model model and password hashed using [`Argon2`](https://en.wikipedia.org/wiki/Argon2) KDF. (`server/db/base.py`)
    + `hash` : Checks user provided credentials and if user is authenticated, the given value is hashed and returned.
6. Client receives and parses the response.

    
Message are using [`msgpack`](https://msgpack.org/index.html) as binary serialization format. All messages are passed around in binary by default.