"""
Key loading utility
"""

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization

import pathlib


def load_private_key(path):
    with open(path, "rb") as key_file:
        return serialization.load_pem_private_key(
            key_file.read(),
            password=None,
            backend=default_backend()
        )


def load_public_key(path):
    with open(path, "rb") as key_file:
        return serialization.load_pem_public_key(
            key_file.read(),
            backend=default_backend()
        )
