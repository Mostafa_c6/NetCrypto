from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa


def generate_private_key(public_exponent=257, key_size=1024):
    """
    :param public_exponent: default 257
    :param key_size: default 1024
    :return: RSAPrivateKey
    """
    return rsa.generate_private_key(
        public_exponent=public_exponent,
        key_size=key_size,
        backend=default_backend()
    )
