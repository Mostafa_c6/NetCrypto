from cryptography.hazmat.primitives import serialization


def serialize_private_key(private_key):
    """
    :param private_key: RSAPrivateKey instance
    :return: *binary* private key serialization
    """
    return private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption()
    )


def serialize_public_key(public_key):
    """

    :param public_key: RSAPublicKey instance
    :return: *binary* public key serialization
    """
    return public_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    )
