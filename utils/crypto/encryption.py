from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding


def public_key_encryption(public_key, message):
    """
    :param public_key: RSAPublicKey instance
    :param message: *binary* message
    :return: encrypted binary ciphertext
    """
    return public_key.encrypt(
        message,
        padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA1()),
            algorithm=hashes.SHA1(),
            label=None
        )
    )


def private_key_decryption(private_key, ciphertext):
    """
    :param private_key: RSAPrivateKey instance
    :param ciphertext: encrypted *binary* ciphertext
    :return:
    """
    return private_key.decrypt(
        ciphertext,
        padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA1()),
            algorithm=hashes.SHA1(),
            label=None
        )
    )
