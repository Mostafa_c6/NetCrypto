import msgpack
from cryptography.fernet import Fernet
from cryptography.hazmat.primitives.asymmetric import rsa

from utils.crypto.encryption import public_key_encryption, private_key_decryption
from utils.crypto.loading import load_private_key, load_public_key
from abc import ABC, abstractmethod


class Encoding(ABC):
    pass


class DictEncoding(Encoding):

    @classmethod
    def encode(cls, key, data):
        return {b'key': key, b'data': data}

    @classmethod
    def decode(cls, encoded):
        return encoded.get(b'key'), encoded.get(b'data')


class Encryption(ABC):
    @abstractmethod
    def encrypt(self, data: bytes):
        pass

    @abstractmethod
    def decrypt(self, cipherbytes):
        pass


class Symmetric(Encryption):
    pass


class Asymmetric(Encryption):
    pass


class RSA(Asymmetric):
    def __init__(self):
        self.privkey = None
        self.pubkey = None

    def encrypt(self, data: bytes):
        if not self.pubkey:
            raise TypeError("Public key not provided.")
        return public_key_encryption(self.pubkey, data)

    def decrypt(self, cipherbytes):
        if not self.privkey:
            raise TypeError("Private key not provided.")
        return private_key_decryption(self.privkey, cipherbytes)


class DummyAsymmetric(Asymmetric):
    def __init__(self, priv):
        self.priv = priv

    def encrypt(self, data: bytes):
        return data.hex()

    def decrypt(self, cipherbytes):
        return bytes.fromhex(cipherbytes)


class CrappyPGP(object):
    def __init__(self, symmetric, asymmetric, encoding):
        self.symmetric = symmetric
        self.asymmetric = asymmetric
        self.encoding = encoding

    def encrypt(self, data: bytes):
        temporal_key = self.symmetric.generate_key()
        encrypted_key = self.asymmetric.encrypt(temporal_key)

        f = self.symmetric(temporal_key)
        encrypted_data = f.encrypt(data)

        return self.encoding.encode(key=encrypted_key, data=encrypted_data)

    def decrypt(self, data: dict):
        encrypted_key, encrypted_data = self.encoding.decode(data)

        key = self.asymmetric.decrypt(encrypted_key)
        f = self.symmetric(key)
        data = f.decrypt(encrypted_data)
        return data


def pgp_client_side(server_pubkey):
    """
    pgp layer for utils.message on client (encryption)
    :return: pgp instance with Fernet and RSA
    """
    client_rsa = RSA()
    client_rsa.pubkey = load_public_key(server_pubkey)

    return CrappyPGP(symmetric=Fernet, asymmetric=client_rsa, encoding=DictEncoding())


def pgp_server_side(server_privkey):
    """
    pgp layer for utils.message on server (decryption)
    :return: pgp instance with Fernet and RSA
    """
    server_rsa = RSA()
    server_rsa.privkey = load_private_key(server_privkey)

    return CrappyPGP(symmetric=Fernet, asymmetric=server_rsa, encoding=DictEncoding())


if __name__ == '__main__':
    pass
