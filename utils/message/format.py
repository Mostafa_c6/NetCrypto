import msgpack

from utils.message.message import Message


def _serialize(data):
    return msgpack.packb(data)


def _deserialize(data):
    return msgpack.unpackb(data)


def loads(packed_data: bytes):
    """

    :param packed_data: msgpack.packed instance
    :return: unpacked to Message instance
    """
    return Message(_deserialize(packed_data))


def dumps(message: Message):
    """
    """
    return _serialize(message)


def test(a):
    """
    >>> test(3)
    4
    """
    return a + 1


if __name__ == '__main__':
    m = Message({b"key": b"123", b"data": b"secret"})
    client_dumped = dumps(m)
    server_loaded = loads(client_dumped)
    assert server_loaded == m
