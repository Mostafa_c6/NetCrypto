from utils.message.CrappyPGP import *
from unittest import TestCase
from os import getenv


class TestCrappyPGP(TestCase):

    def encrypt_assertion(self, pgp, plaintext):
        cipher = pgp.encrypt(plaintext)
        self.assertTrue(b'key' in cipher)
        self.assertTrue(b'data' in cipher)
        return cipher

    def decrypt_assertion(self, pgp, plaintext):
        cipher = pgp.encrypt(plaintext)
        decrypted = pgp.decrypt(cipher)
        self.assertEqual(decrypted, plaintext)
        return cipher, decrypted

    def test_dummy_encrypt(self):
        pgp = CrappyPGP(symmetric=Fernet, asymmetric=DummyAsymmetric(b'secret priv key'), encoding=DictEncoding())
        self.encrypt_assertion(pgp, b"whatever text")

    def test_dummy_decrypt(self):
        pgp = CrappyPGP(symmetric=Fernet, asymmetric=DummyAsymmetric(b'secret priv key'), encoding=DictEncoding())

        plaintext = b"my plaintext here"
        self.decrypt_assertion(pgp, plaintext)

    def test_rsa_encrypt(self):
        rsa = RSA()

        rsa.pubkey = load_public_key(getenv('CLIENT_PUBLIC_PATH'))
        pgp = CrappyPGP(symmetric=Fernet, asymmetric=rsa, encoding=DictEncoding())
        self.encrypt_assertion(pgp, b"encrypt this text please")

    def test_rsa_decrypt(self):
        encrypter = RSA()
        encrypter.pubkey = load_public_key(getenv('CLIENT_PUBLIC_PATH'))
        pgp = CrappyPGP(symmetric=Fernet, asymmetric=encrypter, encoding=DictEncoding())
        plaintext = b"my plaintext here"

        cipher = pgp.encrypt(plaintext)

        decrypter = RSA()  # generated on the receiving side

        decrypter.privkey = load_private_key(getenv('CLIENT_PRIVATE_PATH'))
        pgp = CrappyPGP(symmetric=Fernet, asymmetric=decrypter, encoding=DictEncoding())

        decrypted = pgp.decrypt(cipher)
        self.assertEqual(decrypted, plaintext)

    def test_pgp_wrappers(self):
        client = pgp_client_side(getenv('CLIENT_PUBLIC_PATH'))
        plaintext = b"value to encrypt"

        cipher = client.encrypt(plaintext)

        server = pgp_server_side(getenv('CLIENT_PRIVATE_PATH'))
        decrypted = server.decrypt(cipher)
        self.assertEqual(decrypted, plaintext)
