import asyncio
from server.response import handle_request
from utils.crypto.encryption import private_key_decryption
from utils.crypto.loading import load_public_key
import utils.message.format as msg_format


def get_server_private_key():
    return load_public_key('server/pair/public.pem')


class NetCryptoServerClientProtocol(asyncio.Protocol):
    def connection_made(self, transport):
        peername = transport.get_extra_info('peername')
        print('Connection from {}'.format(peername))
        self.transport = transport

    def data_received(self, data):
        # loaded_data = msg_format.deserialize(data)
        # decrypted = private_key_decryption(server_key, loaded_data)
        message = msg_format.loads(data)

        print('Data received: {!r}'.format(message))

        response = handle_request(message)
        self.transport.write(msg_format.dumps(response))

        print('Close the client socket.')
        self.transport.close()


server_key = get_server_private_key()

loop = asyncio.get_event_loop()

coro = loop.create_server(NetCryptoServerClientProtocol, '127.0.0.1', 8888)
server = loop.run_until_complete(coro)

# Serve requests until Ctrl+C is pressed
print('Serving on {}'.format(server.sockets[0].getsockname()))
try:
    loop.run_forever()
except KeyboardInterrupt:
    print("Shutting down server...")

# Close the server
server.close()
loop.run_until_complete(server.wait_closed())
loop.close()
print("Server closed.")
