from hashlib import sha256

from server.db.base import auth_user, create_user
from utils.message.constants import USERNAME, PASSWORD, HASH, PUBLIC_KEY
from utils.message.message import Message


def hash_handler(message):
    errors = list()
    hashed_value = str()
    try:
        username = message[USERNAME].decode()
        password = message[PASSWORD].decode()
        if auth_user(username=username, password=password):
            hashed_value = sha256(message[HASH]).hexdigest()
        else:
            errors.append('invalid username/password combination')

    except Exception as ex:
        print(format(ex))
        errors.append(format(ex))
    return Message(errors=errors, hash=hashed_value)


def register_handler(message):
    """
    :param message: handle register message
    :return:
    """
    errors = list()
    created = False
    try:
        username = message[USERNAME].decode()
        password = message[PASSWORD].decode()
        public_key = message[PUBLIC_KEY].decode()

        created, error = create_user(username, password, public_key)
        errors.append(error)

    except Exception as ex:
        print(format(ex))
        errors.append(format(ex))

    return Message(created=created, errors=errors)