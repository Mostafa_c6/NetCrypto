from server.handlers import hash_handler, register_handler
from utils.message.constants import ACTION, HASH, REGISTER

from utils.message.message import Message


def handle_request(message):
    """
    dispatches proper handler for the request
    :param message: incoming loaded Message
    :return:
    """
    print(message)
    action = message[ACTION]
    if action == REGISTER:
        return register_handler(message)
    elif action == HASH:
        return hash_handler(message)
    else:
        return Message(errors=['invalid request'])
