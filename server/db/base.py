from server.db.user import User
from peewee import DoesNotExist

from server.db.pass_ops import hash_password


def db_test():
    try:
        user = User.get(User.username == 'foobar')
        print(user.password, user.public_key, user.join_date)
        # print(user.authenticate('s3cret'))
    except DoesNotExist:

        print("user does not exist")


db_test()


def create_user(username, password, public_key):
    query = User.select().where(User.username == username)
    if query.exists():
        return False, 'username already taken'
    User.create(username=username, password=hash_password(password), public_key=public_key)
    return True, None


def auth_user(username, password):
    try:
        user = User.get(User.username == username)
        if user.authenticate(password):
            return True
        # wrong pass
        return False
    except DoesNotExist:
        # username doesn't exist
        return False


if __name__ == '__main__':
    print(auth_user('foobar', 's3cret'))
    print(auth_user('foddobar', 'dds3cret'))
    print(auth_user('foobar', 'sdf'))
