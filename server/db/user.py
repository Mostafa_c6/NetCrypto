from datetime import datetime
from server.db.pass_ops import check_password
from peewee import *
import os

database = SqliteDatabase(os.environ.get('DATABASE_PATH'))


class BaseModel(Model):
    class Meta:
        database = database


class AuthMixin(object):
    def authenticate(self, given_password):
        raise NotImplemented("Method not implemented")


class User(BaseModel, AuthMixin):
    username = CharField(unique=True)
    password = CharField()  # argon2 hashed password
    public_key = TextField()

    join_date = DateTimeField(default=datetime.now())

    def authenticate(self, given_password):
        return check_password(given_password, self.password)


def create_tables():
    """
    To initiate the database,
    call only once
    """
    with database.connection_context():
        database.create_tables([User])