"""
# Pass Ops
Abstraction layer for password hashing
and verifying
Using Argon2 as the key derivation function
"""
from passlib.hash import argon2


def hash_password(password):
    return argon2.hash(password)


def check_password(password, _hash):
    """
    params:
    @password: password to check against @_hash
    """
    return argon2.verify(password, _hash)
